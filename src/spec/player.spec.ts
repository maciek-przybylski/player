import { AudioPlayer } from '../player/index'
import { Song } from '../player/interfaces/song'

describe('Player', () => {
  const emptyOnPlay = jest.fn()
  const emptyOnPause = jest.fn()
  const emptyOnStop = jest.fn()
  const songs: Song[] = [
    {
      title: 'Tarantelle, Op. 43',
      songDesc: '',
      author: 'Frédéric Chopin',
      pic: 'assets/5938870365_a0c951015b_o.jpg',
      songUrl: 'https://en.wikipedia.org/wiki/Tarantelle_(Chopin)',
      songMp3: 'assets/chopin-tarantelle-op43.mp3',
    },
  ]
  const songsDouble: Song[] = [
    {
      title: 'Tarantelle, Op. 43',
      songDesc: '',
      author: 'Frédéric Chopin',
      pic: 'assets/5938870365_a0c951015b_o.jpg',
      songUrl: 'https://en.wikipedia.org/wiki/Tarantelle_(Chopin)',
      songMp3: 'assets/chopin-tarantelle-op43.mp3',
    },
    {
      title: 'Tarantelle, Op. 43',
      songDesc: '',
      author: 'Frédéric Chopin',
      pic: 'assets/5938870365_a0c951015b_o.jpg',
      songUrl: 'https://en.wikipedia.org/wiki/Tarantelle_(Chopin)',
      songMp3: 'assets/chopin-tarantelle-op43.mp3',
    },
  ]
  let player: AudioPlayer

  beforeEach(() => {
    player = new AudioPlayer({
      onPlay: emptyOnPlay,
      onPause: emptyOnPause,
      onStop: emptyOnStop,
    })
  })
  xit('Should create player', () => {
    player = new AudioPlayer({
      onPlay: emptyOnPlay,
      onPause: emptyOnPause,
      onStop: emptyOnStop,
    })
    expect(player).toBeTruthy()
    expect(player.songsLength).toEqual(0)
  })
  xit('Should update playlist when init playlist is passed', () => {
    expect(player.songsLength).toEqual(1)
    // let songs = player.initPlaylist(songsDouble);
    expect(songs).toEqual(2)
  })
  xit('Should play indexed song from array', async () => {
    expect(player.songsLength).toEqual(1)
    let running = await player.playSong(0)
    expect(running).toEqual(true)
  })
  xit('Should stop plaing sound when next one is selected', async () => {
    // player = new Player({}, songsDouble);
    expect(player.songsLength).toEqual(2)
    expect(await player.playSong(0)).toBeTruthy()
    expect(await player.playSong(1)).toBeTruthy()
  })
  xit('Should return false when invalid index is passed to play function', async () => {
    let running = await player.playSong(10)
    expect(running).toBeFalsy()
  })
  xit('Should return true when valid index is passed to play function', async () => {
    let running = await player.playSong(0)
    expect(songsDouble[0].sound).toBeFalsy()
    expect(running).toBeTruthy()
    // expect(emptyOnPlay).toHaveBeenCalled();
  })
})
