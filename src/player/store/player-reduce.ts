import { Reducer, Store } from 'redux'
import { Song } from '../interfaces/song'
import {
  PlayerActionTypes,
  PlayerActions,
  LoadPlaylist,
} from './player-actions'

export interface PlayerState {
  isPlaing?: boolean
  isPaused?: boolean
  isLoading?: boolean
  currentSoundID?: number
  playlist?: Song[]
  duration?: number
  songToPlayIndex?: number
}

const initialState = {
  isPlaing: false,
  playlist: [],
}

export const reduce: Reducer<PlayerState> = (
  state: PlayerState = initialState,
  action: PlayerActions,
): PlayerState => {
  switch (action.type) {
    case PlayerActionTypes.LoadPlaylist:
      return {
        ...state,
        playlist: action.payload.playlist.map(song => {
          return { ...song }
        }),
      }
    case PlayerActionTypes.Pause: {
      return {
        ...state,
        isLoading: false,
        isPlaing: false,
        isPaused: true,
        songToPlayIndex: undefined,
      }
    }
    case PlayerActionTypes.Play: {
      return {
        ...state,
        isPlaing: true,
        isLoading: false,
        currentSoundID: action.payload.soundID,
        duration: action.payload.duration,
        songToPlayIndex: undefined,
      }
    }
    case PlayerActionTypes.RequestPlat: {
      return {
        ...state,
        isPlaing: false,
        isLoading: true,
        songToPlayIndex: action.payload.index,
      }
    }
    case PlayerActionTypes.SkipTo: {
      return {
        ...state,
        isLoading: false,
        currentSoundID: action.payload.index,
        songToPlayIndex: action.payload.index,
        duration: 0,
      }
    }
  }
  return state
}
