import { reduce } from './reduce'
import { Play, Pause, Next, LoadPlaylist } from './actions'
import { Song } from '../interfaces/song'

const playlist: Song[] = [
  {
    title: 'Tarantelle, Op. 43',
    songDesc: '',
    author: 'Frédéric Chopin',
    pic: 'assets/5938870365_a0c951015b_o.jpg',
    songUrl: 'https://en.wikipedia.org/wiki/Tarantelle_(Chopin)',
    songMp3: 'assets/chopin-tarantelle-op43.mp3',
  },
  {
    title: "Mariage d'amour",
    songDesc: '',
    author: 'Frédéric Chopin',
    pic: 'assets/5966942286_682dcb45b6_o.jpg',
    songUrl: 'https://en.wikipedia.org/wiki/Mariage_d%27amour',
    songMp3: 'assets/chopin-spring.mp3',
  },
]

describe('Actions', () => {
  describe('Play Action', () => {
    it('should create proper object', () => {
      const play = new Play({ soundID: 1002, duration: 1900 })
      expect(play).toEqual({
        type: '[Player Action] PLAY',
        payload: { soundID: 1002, duration: 1900 },
      })
    })
  })
  describe('Pause Action', () => {
    it('should create proper object', () => {
      const play = new Pause()
      expect(play).toEqual({ type: '[Player Action] PAUSE' })
    })
  })
  describe('Next Action', () => {
    it('should create proper object', () => {
      const play = new Next()
      expect(play).toEqual({ type: '[Player Action] NEXT' })
    })
  })
  describe('Load Action', () => {
    it('should create proper object', () => {
      const play = new LoadPlaylist({ playlist: playlist })
      expect(play).toEqual({ type: '[Player Action] LOAD', payload: { playlist: playlist } })
    })
  })
})
