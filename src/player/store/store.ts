import { createStore, Store, applyMiddleware, combineReducers } from 'redux'
import { reduce, PlayerState } from './player-reduce'
import { MyMiddleware } from './middleware'
import { uiReduce, UIState } from './ui-reduce'
import { Song } from '../interfaces/song'
import { RingView, PlaylistView } from '../ui'

let rootReducer = combineReducers<State>({ player: reduce, ui: uiReduce })

export const store: Store<State> = createStore<State>(
  rootReducer,
  (<any>window).__REDUX_DEVTOOLS_EXTENSION__ &&
    (<any>window).__REDUX_DEVTOOLS_EXTENSION__(),
  applyMiddleware(MyMiddleware.dispatchObject),
)

export interface State {
  player: PlayerState
  ui: UIState
}
