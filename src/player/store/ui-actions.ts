import { Action } from 'redux'

export enum UIActionTypes {
  ToRingView = '[UI Action] TORING',
  ToPlaylistView = '[UI Action] TOPLAYLIST',
}

export class ToRingView implements Action {
  public readonly type = UIActionTypes.ToRingView
}

export class ToPlaylistView implements Action {
  public readonly type = UIActionTypes.ToPlaylistView
}

export type UIActions = ToRingView | ToPlaylistView
