import { Song } from '../interfaces/song'
import { Action } from 'redux'

export enum PlayerActionTypes {
  LoadPlaylist = '[Player Action] LOAD',
  SkipTo = '[Player Action] SKIP TO',
  Play = '[Player Action] PLAY',
  Pause = '[Player Action] PAUSE',
  Next = '[Player Action] NEXT',
  RequestPlat = '[Player Action] REQUEST PLAY',
}

export class RequestPlay implements Action {
  public readonly type = PlayerActionTypes.RequestPlat
  constructor(public payload: { index: number }) {}
}

export class Play implements Action {
  public readonly type = PlayerActionTypes.Play
  constructor(public payload: { soundID: number; duration: number }) {}
}

export class Pause implements Action {
  public readonly type = PlayerActionTypes.Pause
}

export class Next implements Action {
  public readonly type = PlayerActionTypes.Next
}

export class LoadPlaylist implements Action {
  public readonly type = PlayerActionTypes.LoadPlaylist
  constructor(public payload: { playlist: Song[] }) {}
}

export class SkipTo implements Action {
  public readonly type = PlayerActionTypes.SkipTo
  constructor(public payload: { index: number }) {}
}

export type PlayerActions =
  | Play
  | Pause
  | Next
  | LoadPlaylist
  | RequestPlay
  | SkipTo
