import { MyMiddleware } from './middleware'
import { Next } from './actions'
import { MiddlewareAPI } from 'redux'
import { State } from './reduce'

class Test {
  valueStr
  valueNum
  constructor() {
    this.valueStr = 'sth'
    this.valueNum = 123
  }
}

const TestObj = {
  valueStr: 'sth',
  valueNum: 123,
}

describe('ToObject', () => {
  it('should convert typedobject to plain object', () => {
    const TestClass = new Test()
    const TestClassConverted = MyMiddleware.convert(TestClass)
    expect(TestClass instanceof Test).toBeTruthy()
    expect(TestClassConverted).toEqual(TestObj)
    expect(TestClassConverted instanceof Test).toBeFalsy()
    expect(TestClassConverted instanceof Object).toBeTruthy()
  })
})
