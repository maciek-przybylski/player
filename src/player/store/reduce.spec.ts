import { reduce } from './reduce'
import * as Actions from './actions'
import { Song } from '../interfaces/song'

const playlist: Song[] = [
  {
    title: 'Tarantelle, Op. 43',
    songDesc: '',
    author: 'Frédéric Chopin',
    pic: 'assets/5938870365_a0c951015b_o.jpg',
    songUrl: 'https://en.wikipedia.org/wiki/Tarantelle_(Chopin)',
    songMp3: 'assets/chopin-tarantelle-op43.mp3',
  },
  {
    title: "Mariage d'amour",
    songDesc: '',
    author: 'Frédéric Chopin',
    pic: 'assets/5966942286_682dcb45b6_o.jpg',
    songUrl: 'https://en.wikipedia.org/wiki/Mariage_d%27amour',
    songMp3: 'assets/chopin-spring.mp3',
  },
]

const initialState = {
  isPlaing: false,
  currentSoundID: null,
  playlist: [],
  duration: 0,
}

const playState = {
  isPlaing: true,
  currentSoundID: 1002,
  duration: 1900,
}

const playAction = new Actions.Play({ soundID: 1002, duration: 1900 })
const nextAction = new Actions.Next()
const pauseAction = new Actions.Pause()
const loadAction = new Actions.LoadPlaylist({ playlist: playlist })
const requestPlay = new Actions.RequestPlay({ index: 12 })

describe('Reducer', () => {
  beforeEach(() => {
    // ??
  })
  it('should returned state with changed isPlaing and currentSoundID', () => {
    expect(reduce(initialState, playAction)).toMatchObject({
      isPlaing: true,
      isLoading: false,
      currentSoundID: 1002,
      duration: 1900,
    })
  })
  it('should returned state with changed list of songs', () => {
    const newStore = reduce(initialState, loadAction)
    expect(newStore).toEqual({
      ...initialState,
      playlist: playlist,
    })
    expect(newStore).not.toBe(initialState)
  })
  it('should chain multiple reducers', () => {
    const newState1 = reduce(initialState, loadAction)
    const newState2 = reduce(newState1, requestPlay)
    expect(newState2).toMatchObject({
      isPlaing: false,
      isLoading: true,
      songToPlayIndex: 12,
    })
    expect(initialState).not.toEqual(newState2)
    expect(initialState).not.toEqual(newState1)
    expect(newState1).not.toEqual(newState2)
  })
})
