import { Reducer, Store } from 'redux'
import { Song } from '../interfaces/song'
import { UIActions, UIActionTypes } from './ui-actions'
import { RingView, PlaylistView } from '../ui'

const uiInitialState: UIState = {}

export interface UIState {
  currentView?: typeof RingView | typeof PlaylistView
}

export const uiReduce: Reducer<UIState> = (
  state = uiInitialState,
  action: UIActions,
): UIState => {
  switch (action.type) {
    case UIActionTypes.ToRingView:
      return { currentView: RingView }
    case UIActionTypes.ToPlaylistView:
      return { currentView: PlaylistView }
  }
  return state
}
