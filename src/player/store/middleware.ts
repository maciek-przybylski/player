import { Middleware, Dispatch } from 'redux'
import { State } from './store'

export class MyMiddleware {
  public static convert(any) {
    return Object.assign({}, any)
  }
  public static dispatchObject: Middleware = api => (
    next: Dispatch<State>,
  ) => action => next(MyMiddleware.convert(action))
}
