import { Howl } from 'howler'
import { Song } from '../interfaces/song'
import { store } from '../store/store'
import * as Actions from '../store/player-actions'
import { Unsubscribe } from 'redux'

export class AudioPlayer {
  private _activeSong: Howl
  private _playlist: Song[]
  private _songHowlerId: number

  constructor() {
    this._playlist = store.getState().player.playlist

    store.subscribe(() => {
      const state = store.getState()
      if (this._playlist !== state.player.playlist) {
        this._playlist = state.player.playlist
      }
      if (
        state.player.isLoading ||
        (state.player.songToPlayIndex !== undefined && state.player.isPlaing)
      ) {
        this.playSong(state.player.songToPlayIndex)
      }
    })
  }

  private playSong(id: number) {
    if (id < 0 || id >= this._playlist.length) {
      return
    }
    /**if play was pressed for secound time on the same song
     * pause that song to be able to resome it later
     */
    if (this._activeSong && this._activeSong.playing()) {
      if (this._activeSong === this._playlist[id].sound) {
        this.pauseSong()
        return
      } else {
        this.stopSong()
        delete this._songHowlerId
      }
    }
    if (this._playlist[id].sound && this._songHowlerId) {
      this._activeSong = this._playlist[id].sound
      this._songHowlerId = this._activeSong.play(this._songHowlerId)
    } else {
      this._activeSong = this._playlist[id].sound = new Howl({
        src: this._playlist[id].songMp3,
        html5: true,
        volume: 0,
        onload: () => {
          this._songHowlerId = this._activeSong.play()
        },
        onloaderror: (id, err) => {},
        onend: () => {
          let playerState = store.getState().player
          if (playerState.currentSoundID < playerState.playlist.length - 1) {
            return store.dispatch(
              new Actions.SkipTo({
                index: store.getState().player.currentSoundID + 1,
              }),
            )
          }
          store.dispatch(new Actions.Pause())
          store.dispatch(new Actions.SkipTo({ index: 0 }))
        },
        onstop: soundId => {
          delete this._songHowlerId
        },
        onpause: soundId => {},
        onplay: soundId => {
          store.dispatch(
            new Actions.Play({
              soundID: id,
              duration: this._activeSong.duration(),
            }),
          )
        },
      })
    }
  }
  private pauseSong() {
    this._activeSong.pause()
    store.dispatch(new Actions.Pause())
  }

  private stopSong() {
    this._activeSong.stop()
  }

  public seek(): number {
    if (store.getState().player.isPlaing) {
      return this._activeSong && <number>this._activeSong.seek()
    }
    return -1
  }
}

interface PlayerOptions {
  onPlay?: (id: number, durtion: number) => void
  onPause?: () => void
  onStop?: () => void
}
