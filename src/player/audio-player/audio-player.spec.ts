import configureStore from 'redux-mock-store'

import { store } from '../store'
import { createStore } from 'redux'
import * as Actions from '../store/actions'
import { staticPlaylist } from '../../assets/playlist'

import { AudioPlayer } from './audio-player'

describe('Audio Player', () => {
  let audioPlayer: AudioPlayer
  beforeEach(() => {
    audioPlayer = new AudioPlayer()
    store.dispatch(new Actions.LoadPlaylist({ playlist: staticPlaylist }))
  })
  it('should create player', () => {
    expect(audioPlayer).toBeTruthy()
  })
  it('should get trigger play song when dispatched Load', () => {
    store.dispatch(new Actions.RequestPlay({ index: 0 }))
    expect(audioPlayer.seek()).toBe(-1)
  })
})
