export interface Song {
  title: string
  songDesc: string
  author: string
  songUrl: string
  songMp3: string
  sound?: Howl
}
