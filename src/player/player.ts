import { AudioPlayer } from './audio-player/audio-player'
import { UI } from './ui/'
import { Song } from './interfaces/song'
import { createDivElement } from './ui/helper'
import * as Actions from './store/player-actions'
import { store } from './store/store'

import * as SVG from 'svg.js'

export class Player {
  private audioPlayer: AudioPlayer
  private ui: UI
  //this is initialization of application
  constructor(private containerSelector: string, private playlist: Song[]) {
    // create audio player
    this.audioPlayer = new AudioPlayer()
    const container = document.getElementById(containerSelector)
    container.classList.add('audio-player')
    const playerContent = createDivElement()
    playerContent.className = 'audio-player__player'
    container.appendChild(playerContent)
    // create UI
    this.ui = new UI(
      playerContent,
      this.audioPlayer.seek.bind(this.audioPlayer),
    )
    store.dispatch(new Actions.LoadPlaylist({ playlist: this.playlist }))
  }
}
