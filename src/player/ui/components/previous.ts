import * as SVG from 'svg.js'
import { Button } from './button'
import { ButtonIcon, ButtonIconConst } from './button-icon'
import { Options } from '.'

export class Previous extends ButtonIcon {
  constructor(options: Options) {
    super()
    ButtonIconConst.totalHeight
    ButtonIconConst.totalWidth
    let path = new SVG.Path()
      .plot(
        `M${ButtonIconConst.totalWidth},0L${ButtonIconConst.totalWidth /
          2} ${ButtonIconConst.totalHeight / 2} ${ButtonIconConst.totalWidth} ${
          ButtonIconConst.totalHeight
        } Z`,
      )
      .stroke({ width: 0 })
      .fill('#FAFAFA')
    let rect = new SVG.Rect()
      .width(4)
      .height(ButtonIconConst.totalHeight)
      .stroke({ width: 0 })
      .fill('#FAFAFA')

    this._group
      .add(path)
      .add(path.clone().dmove(-11, 0))
      .add(rect)
      .move(
        options.containerHeight / 2 -
          options.radius +
          options.gap / 2 -
          ButtonIconConst.totalWidth / 2,
        options.containerHeight / 2 - ButtonIconConst.totalHeight / 2,
      )
  }
}
