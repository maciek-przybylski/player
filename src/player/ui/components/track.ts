import * as SVG from 'svg.js'
import { View } from '../view'
import { ANIMATION_LENGTH, Options } from '.'
import { store } from '../../store/store'

const opacity0 = { 'stroke-opacity': 0 }
const opacity1 = { 'stroke-opacity': 1 }

export class Track {
  strokeWidthThik = 5
  strokeWidthHover = 7
  private _trackCircle: SVG.Path
  private _trackLine: SVG.Line
  private _trackLineBegin: { x: number; y: number }
  private _trackLineEnd: { x: number; y: number }
  private _trackCircleBegin: { x: number; y: number }
  private _trackLineMaxLength: number
  private _radius: number
  private _progress = 0
  private _duration = 0
  private _currentSeek = 0

  constructor(
    private svg: SVG.Doc,
    options: Options,
    private seek?: () => number,
  ) {
    store.subscribe(() => {
      this._currentSeek = seek()
      this._duration = store.getState().player.duration || 0
      this.reportProgress()
    })

    this._trackLineBegin = {
      x: options.containerHeight / 2 - options.radius,
      y: options.containerHeight / 2 - options.radius,
    }
    this._trackLineEnd = {
      x: this._trackLineBegin.x + options.radius * 2 * this._progress,
      y: options.containerHeight / 2 - options.radius,
    }
    this._trackCircleBegin = {
      x: options.containerHeight / 2,
      y: options.containerHeight / 2 - options.radius,
    }
    this._trackLineMaxLength = options.radius * 2
    this._radius = options.radius

    this._trackCircle = new SVG.Path()
      .plot(this.getPathCircle())
      .stroke({ width: this.strokeWidthThik, color: options.color })
      .fill('transparent')
    this._trackLine = new SVG.Line()
    this._trackLine.attr(opacity0)
    this._trackLine.fill('transparent')
    this._trackLine
      .plot(
        this._trackLineBegin.x,
        this._trackLineBegin.y,
        this._trackLineEnd.x,
        this._trackLineEnd.y,
      )
      .stroke({ width: this.strokeWidthThik, color: options.color })
    this._trackCircle.mouseover(() => {
      this._trackCircle.stroke({ width: this.strokeWidthHover })
    })

    this._trackCircle.mousedown(event => {
      let path: SVGPathElement = event.path[0]

      console.dir(path)
      console.log(path.getPointAtLength(0))
      let svg: SVGSVGElement = event.path[1]
      let size: number = Math.min(
        svg.height.baseVal.value,
        svg.width.baseVal.value,
      )
      console.log('svg', path)
    })
    this._trackCircle.mouseout(() => {
      this._trackCircle.stroke({ width: this.strokeWidthThik })
    })
  }

  drawCircle() {
    this._trackLine
      .animate(ANIMATION_LENGTH / 2)
      .attr(opacity0)
      .after(() => {
        this._trackLine.remove()
      })
    this.svg.add(this._trackCircle)
    this._trackCircle
      .animate(ANIMATION_LENGTH / 2, '-', ANIMATION_LENGTH / 2)
      .attr(opacity1)
    this._trackCircle.forward()
  }

  drawLine() {
    this._trackCircle
      .animate(ANIMATION_LENGTH / 2)
      .attr(opacity0)
      .after(() => {
        this._trackCircle.remove()
      })
    this.svg.add(this._trackLine)
    this._trackLine
      .animate(ANIMATION_LENGTH / 2, '-', ANIMATION_LENGTH / 2)
      .attr(opacity1)
    this._trackLine.forward()
  }

  private getPathCircle(): string {
    let begin = `M ${this._trackCircleBegin.x} ${this._trackCircleBegin.y} `
    let first = `a ${this._radius} ${this._radius} 0 0 1 `
    let second = `a ${this._radius} ${this._radius} 0 0 1 `
    let third = `a ${this._radius} ${this._radius} 0 0 1 `
    let forth = `a ${this._radius} ${this._radius} 0 0 1 `
    let firstEnd = ` ${this._radius} ${this._radius} `
    let secondEnd = ` ${-this._radius} ${this._radius} `
    let thirdEnd = ` ${-this._radius} ${-this._radius} `
    let forthEnd = ` ${this._radius} ${-this._radius} `
    if (this._progress >= 0 && this._progress <= 0.25) {
      return (
        begin +
        first +
        `${this._radius * Math.sin(360 * this._progress * Math.PI / 180)} ${this
          ._radius -
          this._radius * Math.cos(360 * this._progress * Math.PI / 180)}`
      )
    }
    if (this._progress > 0.25 && this._progress <= 0.5) {
      return (
        begin +
        first +
        firstEnd +
        second +
        ` -${this._radius -
          this._radius * Math.sin(360 * this._progress * Math.PI / 180)} ${this
          ._radius *
          Math.cos(360 * this._progress * Math.PI / 180) *
          -1}`
      )
    }
    if (this._progress > 0.5 && this._progress <= 0.75) {
      return (
        begin +
        first +
        firstEnd +
        second +
        secondEnd +
        third +
        ` ${this._radius *
          Math.sin(360 * this._progress * Math.PI / 180)} -${this._radius +
          this._radius * Math.cos(360 * this._progress * Math.PI / 180)}`
      )
    }
    if (this._progress > 0.75 && this._progress <= 1) {
      return (
        begin +
        first +
        firstEnd +
        second +
        secondEnd +
        third +
        thirdEnd +
        forth +
        ` ${this._radius +
          this._radius * Math.sin(360 * this._progress * Math.PI / 180)} -${this
          ._radius * Math.cos(360 * this._progress * Math.PI / 180)}`
      )
    }
    return (
      begin +
      first +
      firstEnd +
      second +
      secondEnd +
      third +
      thirdEnd +
      forth +
      forthEnd
    )
  }
  private reportProgress() {
    if (this.seek() === -1) {
      if (this._duration === 0) {
        this._progress = 0
        this.plotProgress()
      }
      return
    }
    this._currentSeek = this.seek()
    this._progress = this._currentSeek / this._duration || 0
    this.plotProgress()
    requestAnimationFrame(() => {
      this.reportProgress()
    })
  }

  private plotProgress(): void {
    this._trackCircle.plot(this.getPathCircle())
    this._trackLine.attr({
      x2: this._trackLineBegin.x + this._trackLineMaxLength * this._progress,
    })
  }
}

enum _trackType {
  Circle = 0,
  Line = 0,
}
