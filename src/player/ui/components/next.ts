import * as SVG from 'svg.js'
import { ButtonIcon, ButtonIconConst } from './button-icon'
import { Options } from '.'

export class Next extends ButtonIcon {
  constructor(options: Options) {
    super()
    let path = new SVG.Path()
      .plot(
        `M0,0L${ButtonIconConst.totalWidth / 2} ${ButtonIconConst.totalHeight /
          2} 0 ${ButtonIconConst.totalHeight} Z`,
      )
      .stroke({ width: 0 })
      .fill('#FAFAFA')
    let rect = new SVG.Rect()
      .width(4)
      .height(18)
      .dmove(22, 0)
      .stroke({ width: 0 })
      .fill('#FAFAFA')
    this._group
      .add(path)
      .add(path.clone().dmove(11, 0))
      .add(rect)
      .move(
        options.containerHeight - options.gap / 2 - ButtonIconConst.totalWidth,
        options.containerHeight / 2 - ButtonIconConst.totalHeight / 2,
      )
  }
}
