export * from './play'
export * from './next'
export * from './previous'
export * from './hamburger'
export * from './track'

export const ANIMATION_LENGTH = 250
export const DEFAULT_OPTIONS: Options = {
  containerHeight: 400,
  radius: 185,
  gap: 50,
  color: '#FAFAFA',
}

export interface Options {
  radius: number
  containerHeight: number
  gap?: number
  onClick?: () => void
  color: string
}
