import * as SVG from 'svg.js'
import { Button } from './button'
import { store } from '../../store/store'
import { ToPlaylistView, ToRingView } from '../../store/ui-actions'
import { ANIMATION_LENGTH, Options } from '.'

import { ButtonIcon, ButtonIconConst } from './button-icon'

export class Hamburger extends ButtonIcon {
  private _button: Button
  private _buttonSvg: SVG.Path
  private _line1: SVG.Rect
  private _line2: SVG.Rect
  private _line3: SVG.Rect

  private _state: HamburgerType
  constructor(private options: Options) {
    super()

    this._button = new Button(options).rotate(-45)
    this._buttonSvg = this._button.svgCurve
    this._line1 = this.createLine()
    this._line2 = this.createLine().dmove(0, 7)
    this._line3 = this.createLine().dmove(0, 14)
    this._state = HamburgerType.Hamburger
    ;(<any>this._buttonSvg).off('click')
    this._buttonSvg.on('click', this.animate, this)
    this._group
      .add(
        new SVG.G()
          .add(this._line1)
          .add(this._line2)
          .add(this._line3)
          .fill('#FAFAFA')
          .move(
            options.containerHeight / 2 - ButtonIconConst.totalWidth / 2,
            options.containerHeight / 2 -
              options.radius +
              options.gap / 2 -
              ButtonIconConst.totalHeight / 2,
          ),
      )
      .add(this._buttonSvg)
  }

  private animate() {
    ;(<any>this._buttonSvg).off('click')
    switch (this._state) {
      case HamburgerType.Cross:
        this.changeToHamburger().then(() => {
          this._buttonSvg.on('click', () => {
            this._buttonSvg.front()
            this.animate()
            if (this.options && this.options.onClick) {
              this.options.onClick()
            }
          })
        })
        this._state = HamburgerType.Hamburger
        break
      case HamburgerType.Hamburger:
        this.changeToCross().then(() => {
          this._buttonSvg.on('click', () => {
            this.animate()
            if (this.options && this.options.onClick) {
              this.options.onClick()
            }
          })
        })
        this._state = HamburgerType.Cross
        break
    }
    this.dispatchAction(this._state)
  }

  private createLine(): SVG.Rect {
    return new SVG.Rect()
      .height(4)
      .width(ButtonIconConst.totalWidth)
      .stroke({ width: 0 })
  }

  private changeToCross(): Promise<void> {
    return new Promise((resolve, reject) => {
      function complete() {
        animationCount++
        if (animationCount === 4) {
          resolve()
        }
      }

      let animationCount = 0
      this._line1
        .animate(ANIMATION_LENGTH, '-')
        .attr({ width: ButtonIconConst.diagonal - 2 })
        .rotate(ButtonIconConst.angle2, 2, 2)
        .after(complete.bind(this))
      this._line2
        .animate(ANIMATION_LENGTH)
        .attr('opacity', 0)
        .after(complete.bind(this))
      this._line3
        .animate(ANIMATION_LENGTH)
        .attr({ width: ButtonIconConst.diagonal - 2 })
        .rotate(-ButtonIconConst.angle2, 2, ButtonIconConst.totalHeight - 2)
        .after(complete.bind(this))
      this._buttonSvg
        .animate(ANIMATION_LENGTH / 2)
        .attr('opacity', 0)
        .after(() => {
          this._buttonSvg = this._button.rotate(90).svgStr8.fill('transparent')
          this._buttonSvg
            .animate(ANIMATION_LENGTH / 2)
            .attr('opacity', 1)
            .after(complete.bind(this))
        })
    })
  }

  private changeToHamburger() {
    return new Promise((resolve, reject) => {
      function complete() {
        animationCount++
        if (animationCount === 4) {
          resolve()
        }
      }
      let animationCount = 0
      this._line1
        .animate(ANIMATION_LENGTH, '-')
        .attr({ width: ButtonIconConst.totalWidth })
        .rotate(0, 2, 2)
        .after(complete.bind(this))
      this._line2
        .animate(ANIMATION_LENGTH)
        .attr('opacity', 1)
        .after(complete.bind(this))
      this._line3
        .animate(ANIMATION_LENGTH)
        .attr({ width: ButtonIconConst.totalWidth })
        .rotate(0, 2, ButtonIconConst.totalHeight - 2)
        .after(complete.bind(this))
      this._buttonSvg
        .animate(ANIMATION_LENGTH / 2)
        .attr('opacity', 0)
        .after(() => {
          this._buttonSvg = this._button
            .rotate(-45)
            .svgCurve.fill('transparent')
          this._buttonSvg
            .animate(ANIMATION_LENGTH / 2)
            .attr('opacity', 1)
            .after(complete.bind(this))
        })
    })
  }

  private dispatchAction(type: HamburgerType) {
    type === HamburgerType.Cross
      ? store.dispatch(new ToPlaylistView())
      : store.dispatch(new ToRingView())
  }
}

enum HamburgerType {
  Hamburger = 0,
  Cross = 1,
}
