import * as SVG from 'svg.js'

export abstract class ButtonIcon {
  protected _group = new SVG.G()
  public get svg(): SVG.G {
    return this._group
  }
}

export class ButtonIconConst {
  public static totalWidth = 26
  public static totalHeight = 18
  public static diagonal = 31.622776601683793
  public static angle1 = 57.5288
  public static angle2 = 32.4712
}
