import * as SVG from 'svg.js'
import { Options } from '.'

export class Button {
  protected _button: SVG.Path
  protected _group: SVG.G
  private plotCurveString: string
  private plotStr8String: string
  private plotStr8DashesString: string

  constructor(protected options?: Options) {
    let radius, onClick, gap, containerHeight
    this.options = { radius, onClick, gap, containerHeight } = {
      radius: 150,
      gap: 50,
      onClick: () => {},
      ...options,
    }
    this.plotCurveString = `
    M 0 0 
    a ${radius} ${radius} 0 0 1 ${radius} ${radius} 
    l -${gap}, 0 
    A ${radius - gap} ${radius - gap} 0 0 0 0 ${gap} 
    L 0 0 Z`

    this.plotStr8String = `M 0 0 l 0 ${radius * 2} ${gap} 0 0 ${-radius *
      2} ${-gap} 0 Z`
    this.plotStr8DashesString = `${radius * 2} ${gap} ${radius * 2} ${gap}`

    this._button = new SVG.Path()
    this._button.mouseover(() => {
      this._button.fill('#757575')
      this._button.opacity(0.3)
    })
    this._button.mouseout(() => {
      this._button.fill('transparent')
      this._button.opacity(1)
    })
    this._group = new SVG.G()
    this._button.addClass('ring-view__button')
    this._button.fill('transparent')
    this._button.on('click', () => {
      if (options && options.onClick) {
        options.onClick()
      }
    })
  }

  public get svgCurve(): SVG.Path {
    return this._button
      .plot(this.plotCurveString)
      .stroke({ dasharray: '' })
      .dmove(
        this.options.containerHeight / 2,
        this.options.containerHeight / 2 - this.options.radius,
      )
  }
  public get svgStr8(): SVG.Path {
    return this._button
      .plot(this.plotStr8String)
      .stroke({ dasharray: this.plotStr8DashesString })
      .dmove(
        this.options.containerHeight / 2 - this.options.radius,
        this.options.containerHeight / 2 - this.options.radius,
      )
  }

  public rotate(deg: number): Button {
    this._button.rotate(
      deg,
      this.options.containerHeight / 2,
      this.options.containerHeight / 2,
    )
    return this
  }
}
