import * as SVG from 'svg.js'
import { Options } from '.'
import { ButtonIcon, ButtonIconConst } from './button-icon'

export class Play extends ButtonIcon {
  constructor(options: Options) {
    super()
    let path = new SVG.Path()
      .plot(
        `M0,0L${ButtonIconConst.totalWidth / 2} ${ButtonIconConst.totalHeight /
          2} 0 ${ButtonIconConst.totalHeight} Z`,
      )
      .stroke({ width: 0 })
      .addClass('buttonPlay')
    let rect = new SVG.Rect()
      .width(4)
      .height(ButtonIconConst.totalHeight)
      .dmove(16, 0)
      .stroke({ width: 0 })

    this._group
      .add(path)
      .add(rect)
      .add(rect.clone().dmove(6, 0))
      .fill('#FAFAFA')
      .move(
        options.containerHeight / 2 - ButtonIconConst.totalWidth / 2,
        options.containerHeight / 2 +
          options.radius -
          options.gap / 2 -
          ButtonIconConst.totalHeight / 2,
      )
  }
}
