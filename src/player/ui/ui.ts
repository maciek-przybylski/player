import { View } from './view'
import { Song } from '../interfaces/song'
import { RingView } from './ring-view'
import { PlaylistView } from './playlist-view'
import { store } from '../store/store'

import * as SVG from 'svg.js'
import { Hamburger } from './components/hamburger'
import { Track } from './components/track'
import { Play } from './components/play'
import * as Actions from '../store/player-actions'
import { createDivElement } from './helper'
import { ANIMATION_LENGTH, DEFAULT_OPTIONS } from './components'

const classList = [
  'audio-player--background-1',
  'audio-player--background-2',
  'audio-player--background-3',
  'audio-player--background-4',
]

export class UI {
  private _activeDisplay: RingView | PlaylistView
  private _ringView: RingView
  private _playlistView: PlaylistView
  private _svgContainer: SVG.G
  private _htmlContainer: HTMLElement
  private _currentSong = 0
  private _track: Track

  constructor(container: HTMLElement, private seek?: () => number) {
    this._ringView = new RingView(seek)
    this._playlistView = new PlaylistView(seek)
    this._activeDisplay = this._ringView
    this._svgContainer = this._svgContainer || new SVG.G()
    this._htmlContainer = this._htmlContainer || createDivElement()
    container.classList.add('audio-player--background-1')
    store.subscribe(() => {
      if (this._currentSong !== store.getState().player.currentSoundID) {
        this._currentSong = store.getState().player.currentSoundID || 0
        container.classList.remove(...classList)
        container.classList.add(classList[this._currentSong])
      }
      this.changeView(store.getState().ui.currentView)
    })

    let player = SVG(container).viewbox(
      0,
      0,
      DEFAULT_OPTIONS.containerHeight,
      DEFAULT_OPTIONS.containerHeight,
    )
    player
      .rect(DEFAULT_OPTIONS.containerHeight, DEFAULT_OPTIONS.containerHeight)
      .fill('transparent')
      .stroke({ color: 'transparent', width: 0 })
    player.stroke({ width: 1, color: DEFAULT_OPTIONS.color })

    this._track = new Track(player, DEFAULT_OPTIONS, seek)
    let hamburger = new Hamburger(DEFAULT_OPTIONS).svg
    hamburger
      .opacity(0)
      .animate(ANIMATION_LENGTH / 2, '-', ANIMATION_LENGTH / 2)
      .attr({ opacity: 1 })
    player.add(hamburger)
    player.add(this._svgContainer)
    container.appendChild(this._htmlContainer)
    this.render()
    this._track.drawCircle()
  }

  public changeView(view: typeof RingView | typeof PlaylistView) {
    let renderRequired = false
    switch (view) {
      case RingView:
        if (this._activeDisplay !== this._ringView) {
          this._activeDisplay = this._ringView
          this._track.drawCircle()
          renderRequired = true
        }
        break
      case PlaylistView:
        if (this._activeDisplay !== this._playlistView) {
          this._activeDisplay = this._playlistView
          this._track.drawLine()
          renderRequired = true
        }
        break
    }
    if (renderRequired) {
      this.render()
    }
  }

  private render() {
    this._activeDisplay.render(this._svgContainer, this._htmlContainer)
  }
}
