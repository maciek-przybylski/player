import { RingView } from './ring-view'
import * as Actions from '../store/actions'
import { store } from '../store/store'
import * as SVG from 'svg.js'

let staticPlaylist = [
  {
    title: 'Tarantelle, Op. 43',
    songDesc: '',
    author: 'Frédéric Chopin',
    pic: 'assets/5938870365_a0c951015b_o.jpg',
    songUrl: 'https://en.wikipedia.org/wiki/Tarantelle_(Chopin)',
    songMp3: 'assets/chopin-tarantelle-op43.mp3',
  },
  {
    title: "Mariage d'amour",
    songDesc: '',
    author: 'Frédéric Chopin',
    pic: 'assets/5966942286_682dcb45b6_o.jpg',
    songUrl: 'https://en.wikipedia.org/wiki/Mariage_d%27amour',
    songMp3: 'assets/chopin-spring.mp3',
  },
]

const loadAction = new Actions.LoadPlaylist({ playlist: staticPlaylist })
const requestPlat = new Actions.RequestPlay({ index: 0 })
const playAction = new Actions.Play({ soundID: 123, duration: 111 })
let container: SVG.G
describe('Ring View', () => {
  let ringView: RingView
  beforeEach(() => {
    ringView = new RingView(() => 3)
    store.dispatch(loadAction)
    window.requestAnimationFrame = jest.fn()
    container = new SVG.G()
  })
  it('should create component', () => {
    expect(ringView).toBeTruthy()
  })

  it('render something on the screen', () => {
    ringView.render(container)
    expect(container('button')).toHaveLength(3)
    expect(window.requestAnimationFrame).toHaveBeenCalled()
  })
  it('should change state when clicked play', () => {
    ringView.render(container)
    container.querySelectorAll('button')[1].click()
    expect(store.getState().songToPlayIndex).toBe(0)
  })
  it('should change state when clicked play and next', () => {
    ringView.render(container)
    container.querySelectorAll('button')[1].click()
    expect(store.getState().songToPlayIndex).toBe(0)
    container.querySelectorAll('button')[2].click()
    expect(store.getState().songToPlayIndex).toBe(1)
    container.querySelectorAll('button')[2].click()
    expect(store.getState().songToPlayIndex).toBe(0)
  })
  it('should change state when clicked play and next', () => {
    ringView.render(container)
    container.querySelectorAll('button')[1].click()
    expect(store.getState().songToPlayIndex).toBe(0)
    container.querySelectorAll('button')[0].click()
    expect(store.getState().songToPlayIndex).toBe(1)
  })
})
