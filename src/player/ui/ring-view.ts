import { View } from './view'
import { Song } from '../interfaces/song'
import { createDivElement, createButtonElement } from './helper'
import { store } from '../store/store'
import * as Actions from '../store/player-actions'
import * as SVG from 'svg.js'

import {
  Play,
  Next,
  Previous,
  ANIMATION_LENGTH,
  DEFAULT_OPTIONS,
} from './components'
import { Button } from './components/button'

export class RingView implements View {
  private _playlist: Song[]
  private _activeSong = 0
  private _container: HTMLElement
  private _playPauseButton: SVG.G
  private _skipNext: SVG.G
  private _skipPrev: SVG.G
  private _timer: HTMLDivElement
  private _duration: HTMLDivElement
  private _author: HTMLDivElement
  private _title: HTMLDivElement
  private _isPlaying: boolean

  constructor(private seek: () => number) {
    this._timer = this._timer || createDivElement()
    this._timer.className = 'ring-view__timer'
    this._timer.textContent = '00:00'
    this._author = this._author || createDivElement()
    this._author.className = 'ring-view__author'
    this._title = this._title || createDivElement()
    this._title.className = 'ring-view__title'

    this.renderTimer()
    this._duration = createDivElement()
    store.subscribe(() => {
      const state = store.getState().player
      this._playlist = state.playlist
      this._duration.textContent = state.duration && state.duration.toString()
      this._isPlaying = state.isPlaing
      this._activeSong = state.currentSoundID || 0
      this._author.textContent =
        state.playlist && state.playlist[this._activeSong].author
      this._title.textContent =
        state.playlist && state.playlist[this._activeSong].title
      this.renderTimer(store.getState().player.duration)
    })
  }

  public render(svgContainer: SVG.G, htmlContainer?: HTMLElement) {
    this._skipPrev =
      this._skipPrev ||
      new SVG.G().add(new Previous(DEFAULT_OPTIONS).svg).add(
        new Button({
          ...DEFAULT_OPTIONS,
          onClick: () => this.skipPrev(),
        }).rotate(-135).svgCurve,
      )
    this._playPauseButton =
      this._playPauseButton ||
      new SVG.G().add(new Play(DEFAULT_OPTIONS).svg).add(
        new Button({
          ...DEFAULT_OPTIONS,
          onClick: () =>
            store.dispatch(
              new Actions.RequestPlay({ index: this._activeSong }),
            ),
        }).rotate(135).svgCurve,
      )
    this._skipNext =
      this._skipNext ||
      new SVG.G().add(new Next(DEFAULT_OPTIONS).svg).add(
        new Button({
          ...DEFAULT_OPTIONS,
          onClick: () => this.skipNext(),
        }).rotate(45).svgCurve,
      )
    svgContainer
      .animate(ANIMATION_LENGTH / 2)
      .attr({ 'stroke-opacity': 0, 'fill-opacity': 0 })
      .after(() => {
        if (htmlContainer) {
          htmlContainer.innerHTML = ''
          htmlContainer.className = 'ring-view view'
          htmlContainer.appendChild(this._timer)
          htmlContainer.appendChild(this._author)
          htmlContainer.appendChild(this._title)
        }
        svgContainer.clear()
        svgContainer.add(this._playPauseButton)
        svgContainer.add(this._skipPrev)
        svgContainer.add(this._skipNext)
        svgContainer
          .animate(ANIMATION_LENGTH / 2)
          .attr({ 'stroke-opacity': 1, 'fill-opacity': 1 })
      })
  }

  private renderTimer(duration?: number) {
    if (duration === 0) {
      this._timer.textContent = `${this.parseTime(0)}`
    }
    if (this._timer && this.seek && this._isPlaying) {
      const state = store.getState()
      this._timer.textContent = `${this.parseTime(this.seek())}`
      requestAnimationFrame(() => {
        this.renderTimer()
      })
    }
  }

  private parseTime(seek: number): string {
    let minutes = '' + Math.floor(seek / 60)
    if (minutes.length === 1) {
      minutes = '0' + minutes
    }
    let seconds = '' + Math.floor(seek % 60)
    if (seconds.length === 1) {
      seconds = '0' + seconds
    }
    return `${minutes}:${seconds}`
  }

  private skipTo(index: number) {
    this._activeSong = index
    store.dispatch(new Actions.SkipTo({ index }))
  }

  private skipNext() {
    this._activeSong++
    if (this._activeSong === this._playlist.length) {
      this._activeSong = 0
    }
    return this.skipTo(this._activeSong)
  }
  private skipPrev() {
    this._activeSong--
    if (this._activeSong === -1) {
      this._activeSong = this._playlist.length - 1
    }
    return this.skipTo(this._activeSong)
  }
}
