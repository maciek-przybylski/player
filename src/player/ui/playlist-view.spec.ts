import { PlaylistView } from './playlist-view'
import * as Actions from '../store/actions'
import { store } from '../store/store'

let staticPlaylist = [
  {
    title: 'Tarantelle, Op. 43',
    songDesc: '',
    author: 'Frédéric Chopin',
    pic: 'assets/5938870365_a0c951015b_o.jpg',
    songUrl: 'https://en.wikipedia.org/wiki/Tarantelle_(Chopin)',
    songMp3: 'assets/chopin-tarantelle-op43.mp3',
  },
  {
    title: "Mariage d'amour",
    songDesc: '',
    author: 'Frédéric Chopin',
    pic: 'assets/5966942286_682dcb45b6_o.jpg',
    songUrl: 'https://en.wikipedia.org/wiki/Mariage_d%27amour',
    songMp3: 'assets/chopin-spring.mp3',
  },
]

const loadAction = new Actions.LoadPlaylist({ playlist: staticPlaylist })
const requestPlat = new Actions.RequestPlay({ index: 0 })
const playAction = new Actions.Play({ soundID: 123, duration: 111 })
let container: HTMLDivElement
describe('Playlist View', () => {
  let playlist
  beforeEach(() => {
    playlist = new PlaylistView(() => 3)
    store.dispatch(loadAction)
    container = document.createElement('div')
  })
  it('should create playlist view', () => {
    expect(playlist).toBeTruthy()
  })
  it('should create playlist view', () => {
    playlist.render(container)
    expect(container.querySelectorAll('div.playlist__item')).toHaveLength(staticPlaylist.length)
  })
})
