export function createButtonElement(
  text: string,
  onclick: () => void,
): HTMLButtonElement {
  let button = <HTMLButtonElement>document.createElement('button')
  button.textContent = text
  button.onclick = onclick
  return button
}

export function createDivElement(
  text?: string,
  onclick?: () => void,
): HTMLDivElement {
  let div = <HTMLDivElement>document.createElement('div')
  div.textContent = text
  div.onclick = onclick
  return div
}

export function createSpanElement(
  text?: string,
  onclick?: () => void,
): HTMLSpanElement {
  let span = <HTMLDivElement>document.createElement('span')
  span.textContent = text
  span.onclick = onclick
  return span
}
