import { Song } from '../interfaces/song'
import { createDivElement, createSpanElement } from './helper'
import { View } from './view'
import { store } from '../store/store'
import * as SVG from 'svg.js'
import { RequestPlay } from '../store/player-actions'
import { ANIMATION_LENGTH } from './components'

export class PlaylistView implements View {
  playlist: Song[]

  constructor(seek: () => number) {
    store.subscribe(() => {
      this.playlist = store.getState().player.playlist.map(song => {
        return { ...song }
      })
    })
  }

  public render(svgContainer: SVG.G, htmlContainer: HTMLElement) {
    htmlContainer.className = 'playlist-view view'
    htmlContainer.innerHTML = ''
    svgContainer
      .animate(ANIMATION_LENGTH / 2)
      .attr({ 'stroke-opacity': 0, 'fill-opacity': 0 })
      .after(() => {
        svgContainer.clear()
        this.playlist.forEach((song, index) => {
          htmlContainer.appendChild(this.createSongRow(song, index))
        })
      })
  }

  private createSongRow(song: Song, index: number): HTMLElement {
    const div = createDivElement()
    div.className = 'playlist-view__list-item'
    div.ondblclick = () => {
      store.dispatch(new RequestPlay({ index }))
    }
    const spanTitle = createSpanElement(song.title)
    spanTitle.className = 'playlist-view__title'
    const spanAuthor = createSpanElement(song.author)
    spanAuthor.className = 'playlist-view__author'
    div.appendChild(spanTitle)
    div.appendChild(spanAuthor)
    return div
  }
}
