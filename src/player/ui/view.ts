import * as SVG from 'svg.js'

export interface View {
  render(svgContainer: SVG.G, htmlContainer?: HTMLElement)
}
