import { staticPlaylist } from './assets/playlist'
import './assets'
import './style/style'
import { Player } from './player'

const player = new Player('player', staticPlaylist)
