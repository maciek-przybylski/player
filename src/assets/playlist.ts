import { Song } from '../player/interfaces/song'

export let staticPlaylist: Song[] = [
  {
    title: 'Tarantelle, Op. 43',
    songDesc: '',
    author: 'Frédéric Chopin',
    songUrl: 'https://en.wikipedia.org/wiki/Tarantelle_(Chopin)',
    songMp3: 'assets/chopin-tarantelle-op43.mp3',
  },
  {
    title: "Mariage d'amour",
    songDesc: "Andantino 'Spring', B. 117",
    author: 'Frédéric Chopin',
    songUrl: 'https://en.wikipedia.org/wiki/Mariage_d%27amour',
    songMp3: 'assets/chopin-spring.mp3',
  },
  {
    title: 'Mazurka in D major, B. 4',
    songDesc: '',
    author: 'Frédéric Chopin',
    songUrl: 'https://en.wikipedia.org/wiki/Mariage_d%27amour',
    songMp3: 'assets/chopin-mazurka-in-d-major-b4.mp3',
  },
  {
    title: 'Mazurka in D major, B. 71',
    songDesc: '',
    author: 'Frédéric Chopin',
    songUrl: 'https://en.wikipedia.org/wiki/Mariage_d%27amour',
    songMp3: 'assets/chopin-mazurka-in-d-major-b71.mp3',
  },
]
